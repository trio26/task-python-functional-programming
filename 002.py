import re


def SnakeCase(words):
    word_snake = re.sub(" ", "_", words)
    return word_snake

hello = SnakeCase("hello world again")
print(hello)