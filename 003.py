import re


def KebabCase(words):
    word_kebab = re.sub(" ", "-", words)
    return word_kebab

hello = KebabCase("hello world again")
print(hello)