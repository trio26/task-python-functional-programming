satuan = ['Nol', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan']
belasan = ['Sepuluh', 'Sebelas', 'Dua Belas', 'Tiga Belas', 'Empat Belas', 'Lima Belas', 'Enam Belas', 'Tujuh Belas', 'Delapan Belas', 'Sembilan Belas', 'Dua Puluh']
puluhan = ['Dua Puluh', 'Tiga Puluh', 'Empat Puluh', 'Lima Puluh', 'Enam Puluh', 'Tujuh Puluh', 'Delapan Puluh', 'Sembilan Puluh', 'Seratus']


def Convert(n):
    if n <=9:
        return satuan[n]
    elif n >= 10 and n <= 19:
        return belasan[n-10]
    elif n >= 20 and n <= 99:
        return puluhan[(n//10)-2] + " " + (satuan[n % 10] if n % 10 !=0 else "")
    elif n >= 100 and n <= 999:
        return Convert(n//100) + " Ratus " + (Convert(n % 100) if n % 100 != 0 else "")
    elif n >= 1000 and n <= 9999:
        return Convert(n//1000) + " Ribu " + (Convert(n % 1000) if n % 1000 != 0 else "")
    elif n >= 10000 and n <= 99999:
        return Convert(n//10000) + " Puluh  " + (Convert(n % 10000) if n % 10000 != 0 else "")
    elif n >= 100000 and n <= 999999:
        return Convert(n//100000) + " Ratus " + (Convert(n % 100000) if n % 100000 != 0 else "")
    elif n >= 1000000 and n <= 9999999:
        return Convert(n//1000000) + " Juta " + (Convert(n % 1000000) if n % 1000000 != 0 else "")
    elif n >= 10000000:
        return Convert(n % 10000000) + " Puluh Juta " + (Convert(n % 10000000) if n % 10000000 != 0 else "")


satu = Convert(1)
belasan = Convert(12)
puluhan = Convert(30)

print(satu)
print(belasan)
print(puluhan)